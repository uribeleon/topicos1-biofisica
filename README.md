Topics in Biophysic 1
==================================
by: **Cesar Alfredo Uribe León**

*University of Antioquia 2016* 

This repository contain a basic information about the postgrade course _Topics in biophysics 1_ and some homeworks related with it.



Homeworks
--------------


### Homework 1

The first homework was taken from the book *Mathematical Models in Biology: An Introduction* 2ed by *Elizabeth Allman*

1. **Project 1 (Page 37)**
    
    [The Ricker model](http://nbviewer.jupyter.org/urls/bitbucket.org/Cesar_Alfredo_Uribe_Leon/topicos1-biofisica/raw/f098c93061fa2bddce59c3daeec7bb79f57fb8ef/Tareas/Tarea1/Tarea1-TopicosBiofisicaI-P1.ipynb)

2. **Project 2 (Page 37)**

    *Modeling an insect population*
    
    [Insect population](http://nbviewer.jupyter.org/urls/bitbucket.org/Cesar_Alfredo_Uribe_Leon/topicos1-biofisica/raw/f098c93061fa2bddce59c3daeec7bb79f57fb8ef/Tareas/Tarea1/Tarea1-TopicosBiofisicaI-P2.ipynb)

3. **Project 2 (Page 77)**

    *Leslie and Usher Model*
    
    [Sensitivity analysis](http://nbviewer.jupyter.org/urls/bitbucket.org/Cesar_Alfredo_Uribe_Leon/topicos1-biofisica/raw/f098c93061fa2bddce59c3daeec7bb79f57fb8ef/Tareas/Tarea1/Tarea1-TopicosBiofisicaI-P3.ipynb)

4. **Project 1 (Page 109)**

    *Competition model for two species that fill the same niche*

    [Competition model](http://nbviewer.jupyter.org/urls/bitbucket.org/Cesar_Alfredo_Uribe_Leon/topicos1-biofisica/raw/e07799836df83d9d01665e5a1a59412756e68e2e/Tareas/Tarea1/Tarea1-TopicosBiofisicaI-P4.ipynb)
